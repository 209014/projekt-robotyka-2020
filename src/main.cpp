#include "ros/ros.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <bits/stdc++.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

// Point cloud library
#include <pcl/pcl_config.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/parse.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "app_options/program_arguments.hpp"
#include "app_options/command_line_arguments.hpp"
#include "recorders/picture_saver.hpp"
#include "imgproc/pcl_utils.hpp"
#include "image_receivers/ros_image2D_listener.hpp"
#include "image_receivers/ros_pointcloud2_listener.hpp"

void Invitation(void);
int ConnectToCamera(int cameraID, cv::Size *frameSize, cv::VideoCapture *cap);
template <typename PointT>
int PrintSegmentInfo(const typename pcl::PointCloud<PointT>::ConstPtr cloud, bool computeDensity);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr SegmentPlanes(pcl::SACSegmentation<pcl::PointXYZRGB> &seg, pcl::PointCloud<pcl::PointXYZRGB> &cloud);

int main(int argc, char **argv)
{
    Invitation();

    AppOptions::CmdArgumentsReader cmdArgsReader(argc, argv);

    if (cmdArgsReader.IsHelpOrUnrecognizedCommandPresent())
    {
        cmdArgsReader.PrintHelpMessage();
        return 0;
    }

    AppOptions::ProgramArguments programArgs;
    cmdArgsReader.Parse(&programArgs);

    /////////////////////////////////////////////////////////////////////////////////////////////////

    boost::shared_ptr<Recorders::PictureSaver> camPictureRecorder;
    boost::shared_ptr<Recorders::PictureSaver> rosRGBPictureRecorder;
    boost::shared_ptr<Recorders::PictureSaver> rosDepthPictureRecorder;

    if (programArgs.recordAllFramesToFiles)
    {
        programArgs.recordCameraFramesToFiles = true;
        programArgs.recordRosRGBFramesToFiles = true;
        programArgs.recordRosDepthFramesToFiles = true;
    }

    if (programArgs.recordCameraFramesToFiles)
    {
        camPictureRecorder = boost::make_shared<Recorders::PictureSaver>("cam", 45000, programArgs.recordEveryNthFrame);
        camPictureRecorder->PrepareSaveDirectory();
        camPictureRecorder->Start();
    }

    if (programArgs.recordRosRGBFramesToFiles)
    {
        rosRGBPictureRecorder = boost::make_shared<Recorders::PictureSaver>("rosRGB", 45000, programArgs.recordEveryNthFrame);
        rosRGBPictureRecorder->PrepareSaveDirectory();
        rosRGBPictureRecorder->Start();
    }

    if (programArgs.recordRosDepthFramesToFiles)
    {
        rosDepthPictureRecorder = boost::make_shared<Recorders::PictureSaver>("rosDepth", 45000, programArgs.recordEveryNthFrame);
        rosDepthPictureRecorder->PrepareSaveDirectory();
        rosDepthPictureRecorder->Start();
    }

    ros::init(argc, argv, "ProjectBase");
    ros::NodeHandle n;
    ImageReceivers::Image2DTopic imageTopicRGB(n, programArgs.rostopicRGBName);
    ImageReceivers::Image2DTopic imageTopicDepth(n, programArgs.rostopicDepthImageName);
    ImageReceivers::CloudTopic cloudTopic(&n, programArgs.rostopicPoincloud2Name);

    cv::VideoCapture cap;

    if (programArgs.retrieveRGBFromRostopic)
        imageTopicRGB.SubscribeToTopic();
    if (programArgs.retrieveDepthImageFromRostopic)
        imageTopicDepth.SubscribeToTopic();
    if (programArgs.retrievePoincloud2Rostopic)
        cloudTopic.SubscribeToTopic();
    if (programArgs.retrieveFromCamera)
    {
        if (!ConnectToCamera(programArgs.cameraID, &programArgs.cameraFrameSize, &cap))
        {
            std::cerr << "Camera capture connection failed." << std::endl;
            programArgs.retrieveFromCamera = false;
        }
    }

    const pcl::PointCloud<pcl::PointXYZRGB>::Ptr segmentedColourizedCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::visualization::PCLVisualizer::Ptr viewerRawCloud = ImgProc::GetViewer("3D rgbVis");
    pcl::visualization::PCLVisualizer::Ptr viewerSegPlanes = ImgProc::GetViewer("3D rgb planar Vis");
    ImgProc::AddCloudToViewer(viewerRawCloud, cloudTopic.GetCloud(), "sample cloud");
    ImgProc::AddCloudToViewer(viewerSegPlanes, segmentedColourizedCloud, "sample cloud");

    pcl::SACSegmentation<pcl::PointXYZRGB> seg = ImgProc::GetSegmentatorPlaneRANSAC<pcl::PointXYZRGB>(0.135, 50);

    while (ros::ok())
    {

        if (programArgs.retrieveFromCamera)
        {
            cv::Mat frameIN;
            if (cap.read(frameIN))
            {
                cv::imshow("webcam", frameIN);
                camPictureRecorder->SaveTask(frameIN);
            }
        }

        if (programArgs.retrievePoincloud2Rostopic)
        {
            if (cloudTopic.IsNewCloudReceived())
            {
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr colourizedSegmentedCloud = SegmentPlanes(seg, *cloudTopic.GetCloud());

                pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbCallback(cloudTopic.GetCloud());
                viewerRawCloud->updatePointCloud<pcl::PointXYZRGB>(cloudTopic.GetCloud(), rgbCallback, "sample cloud");

                pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgbSegmented(colourizedSegmentedCloud);
                viewerSegPlanes->updatePointCloud<pcl::PointXYZRGB>(colourizedSegmentedCloud, rgbSegmented, "sample cloud");
            }

            viewerRawCloud->spinOnce(20);
            viewerSegPlanes->spinOnce(20);

            if (viewerRawCloud->wasStopped() || viewerSegPlanes->wasStopped())
                break;
        }

        if (imageTopicRGB.IsNewImageReceived())
        {
            imageTopicRGB.Display();
            rosRGBPictureRecorder->SaveTask(imageTopicRGB.GetImage());
        }

        if (imageTopicDepth.IsNewImageReceived())
        {
            imageTopicDepth.DisplayHistEqualized(1 / 20.0);
            rosDepthPictureRecorder->SaveTask(imageTopicDepth.GetImage());
        }

        if (cv::waitKey(10) >= 0)
            break;
        ros::spinOnce();
    }

    std::cout << std::endl;

    camPictureRecorder->Close();
    rosRGBPictureRecorder->Close();
    rosDepthPictureRecorder->Close();

    return EXIT_SUCCESS;
}

void 
Invitation(void)
{

    std::cout << std::endl;
    std::cout << "Welcome to RosOpencvViewer app." << std::endl;
    std::cout << "You can use this app to as a starting point of your own app." << std::endl;
    std::cout << "Detected OpenCV version: " << CV_VERSION << std::endl;
    std::cout << std::endl;
    std::cout << "PCL VERSION: " << PCL_VERSION << std::endl;
}

int 
ConnectToCamera(int cameraID, cv::Size *frameSize, cv::VideoCapture *cap)
{
    *cap = cv::VideoCapture(cameraID);
    if (!cap->isOpened())
    {
        std::cout << "VideoCapture error." << std::endl;
        return 0;
    }
    else
    {
        cap->set(cv::CAP_PROP_FRAME_WIDTH, frameSize->width);
        cap->set(cv::CAP_PROP_FRAME_HEIGHT, frameSize->height);
        frameSize->width = cap->get(cv::CAP_PROP_FRAME_WIDTH);
        frameSize->height = cap->get(cv::CAP_PROP_FRAME_HEIGHT);
        std::cout << "VideoCapture opened!" << std::endl;
        std::cout << "Size: " << frameSize->width << "x" << frameSize->height << std::endl;
    }
    return 1;
}

pcl::PointCloud<pcl::PointXYZRGB>::Ptr
SegmentPlanes(pcl::SACSegmentation<pcl::PointXYZRGB> &seg, pcl::PointCloud<pcl::PointXYZRGB> &cloud)
{
    typename pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudToBeSegmented(new pcl::PointCloud<pcl::PointXYZRGB>);
    typename pcl::PointCloud<pcl::PointXYZRGB>::Ptr colourizedSegmentedCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::copyPointCloud(cloud, *cloudToBeSegmented);

    PrintSegmentInfo<pcl::PointXYZRGB>(cloudToBeSegmented, false);

    int i = 0;
    int nr_points = (int)cloudToBeSegmented->points.size();
    while (cloudToBeSegmented->points.size() > 0.2 * nr_points && i <= 7)
    {
        i++;

        pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        seg.setInputCloud(cloudToBeSegmented);
        seg.segment(*inliers, *coefficients);

        if (inliers->indices.size() == 0)
        {
            break;
        }

        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZRGB> extract;
        extract.setInputCloud(cloudToBeSegmented);
        extract.setIndices(inliers);
        extract.setNegative(false);

        // Get the points associated with the planar surface
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPlane(new pcl::PointCloud<pcl::PointXYZRGB>);
        cloudPlane.get()->clear();
        extract.filter(*cloudPlane);

        // Remove the planar inliers, extract the rest
        extract.setNegative(true);
        extract.filter(*cloudToBeSegmented);

        for (int j = 0; j < cloudPlane.get()->points.size(); j++)
        {
            cloudPlane.get()->points.at(j).rgba = ImgProc::GetEasyDistinguishableColor(i).rgba;
        }

        *colourizedSegmentedCloud.get() += *cloudPlane.get();
    }

    return colourizedSegmentedCloud;
}


template <typename PointT>
int
PrintSegmentInfo(const typename pcl::PointCloud<PointT>::ConstPtr cloud, bool computeDensity)
{
    std::cout << "------------------------------------------------------" << std::endl;

    if(computeDensity)
    {
        std::cout << "Cloud resolution: " << ImgProc::ComputeCloudResolution<PointT>(cloud) << std::endl;
    }
    
    std::cout << "temp_cloud_segm: " << cloud.get()->size() << std::endl;
    std::cout << cloud.get()->width << ", isdense = " << (cloud.get()->is_dense ? "true" : "false") << std::endl;
}
template int PrintSegmentInfo<pcl::PointXYZRGB>(const typename pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, bool computeDensity);
template int PrintSegmentInfo<pcl::PointXYZ>(const typename pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud, bool computeDensity);

