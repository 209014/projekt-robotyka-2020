#ifndef ROS_POINTCLOUD2_LISTENER_HPP
#define ROS_POINTCLOUD2_LISTENER_HPP

#include <pcl_conversions/pcl_conversions.h>

namespace ImageReceivers
{
    class CloudTopic
    {
    public:
        CloudTopic(ros::NodeHandle *n, const std::string &topicName);
        void SubscribeToTopic();
        void CloudCallback(const sensor_msgs::PointCloud2 &cloud_msg);
        bool IsNewCloudReceived();
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr GetCloud();

        template <typename PointT>
        static void
        ConvertPointClound2ToPCL(const sensor_msgs::PointCloud2 &pc, const typename pcl::PointCloud<PointT>::Ptr &cloud);

    private:
        bool newCloud = false;
        std::string topicName;
        ros::NodeHandle *n;
        ros::Subscriber sub;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudFromCallback = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGB>>();
    };
}


#endif
