#include <string>
#include <chrono>
#include <sys/stat.h>
#include <sys/types.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include "picture_saver.hpp"

namespace Recorders
{
    PictureSaver::PictureSaver(const std::string &pictureNames, long maxNumberOfPictures, int saveOneInN)
    {
        this->pictureNames = pictureNames;
        this->maxNumberOfRecordedPictures = maxNumberOfPictures;
        this->saveEveryNthPicture = saveOneInN;

        InitDefaults();
        SetSaveDirectoryName("");
        SetPathToSaveDirectory("/uav_recorder");
    }

    void
    PictureSaver::InitDefaults()
    {
        this->appendDateToPictureName = false;
        this->saveDirectoryExists = false;
        this->recording = false;
        this->atLeastOneSaved = false;
        this->atLeastOnceTurnedOn = false;
        this->reachedMaxNumberOfRecordedPictures = false;
        this->numberOfSavedPictured = 0;
        this->unsuccessfullySavedPictures = 0;
        this->pictureSavePrescaler = 0;
    }

    void
    PictureSaver::SetSaveDirectoryName(const std::string &name, bool appendCurrentDatetime)
    {
        saveDirectoryName = "/" + name;
        if (appendCurrentDatetime)
            saveDirectoryName += CurrentDateTime();
    }

    std::string
    PictureSaver::CurrentDateTime(const bool compactFormat)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        //Too long on purpose. I don't want any problems with this ever.
        char buf[128] = {0};

        if (!compactFormat)
            std::strftime(buf, sizeof(buf), "Date_%Y-%m-%d_Time_%H-%M-%S", std::localtime(&now));
        else
            std::strftime(buf, sizeof(buf), "D%Y%m%dT%H%M%S", std::localtime(&now));

        return buf;
    }

    void
    PictureSaver::SetPathToSaveDirectory(const std::string &path, const bool isRelativeToHome)
    {
        if (isRelativeToHome)
            pathToSaveDirectory = getenv("HOME") + path;
        else
            pathToSaveDirectory = path;
    }

    void
    PictureSaver::Start()
    {
        recording = true;
        atLeastOnceTurnedOn = true;
    }

    void
    PictureSaver::Stop()
    {
        recording = false;
    }

    bool
    PictureSaver::PrepareSaveDirectory()
    {
        if (recording)
        {
            std::cerr << "Path cannot be changed while running." << std::endl;
            return false;
        }

        if (atLeastOnceTurnedOn)
        {
            Close();
            atLeastOnceTurnedOn = false;
        }

        fullAbsolutePath = pathToSaveDirectory + saveDirectoryName;
        if (!DirectoryExists(fullAbsolutePath))
        {
            return CreateDirectory(fullAbsolutePath);
        }
        return true;
    }

    bool
    PictureSaver::DirectoryExists(const std::string &path)
    {
        struct stat buffer;
        return (stat(path.c_str(), &buffer) == 0);
    }

    bool
    PictureSaver::CreateDirectory(const std::string &path)
    {
        //Too long on purpose. I don't want any problems with this ever.
        char commandPathBuffer[512];
        sprintf(commandPathBuffer, "mkdir -p %s", path.c_str());
        int status = system(commandPathBuffer);

        if (status == -1)
        {
            std::cerr << "Error : " << strerror(errno) << std::endl;
            return false;
        }
        else
        {
            std::cout << "Directories are created" << std::endl;
            return true;
        }
    }

    void
    PictureSaver::SaveTask(const cv::Mat &picture)
    {
        if (!recording)
        {
            return;
        }
        else if (numberOfSavedPictured >= maxNumberOfRecordedPictures)
        {
            reachedMaxNumberOfRecordedPictures = true;
            return;
        }
        else if (pictureSavePrescaler == 0)
        {
            //Too long on purpose. I don't want any problems with this ever.
            char picturePath[512];

            if (appendDateToPictureName)
                sprintf(picturePath, "%s/%s_%05ld_%s_.jpg", fullAbsolutePath.c_str(), pictureNames.c_str(), numberOfSavedPictured, CurrentDateTime(true).c_str());
            else
                sprintf(picturePath, "%s/%s_%05ld.jpg", fullAbsolutePath.c_str(), pictureNames.c_str(), numberOfSavedPictured);

            bool success = imwrite(picturePath, picture);
            if (success)
            {
                atLeastOneSaved = true;
                numberOfSavedPictured++;
            }
            else
            {
                unsuccessfullySavedPictures++;
            }
        }
        pictureSavePrescaler = ((pictureSavePrescaler + 1) % saveEveryNthPicture);
    }

    void
    PictureSaver::Close()
    {
        Stop();

        if (atLeastOneSaved)
        {
            std::cout << pictureNames << " recorder: Saved " << numberOfSavedPictured << " pictures in: " << fullAbsolutePath << std::endl;
        }
        else
        {
            if(atLeastOnceTurnedOn)
            {
                std::string msg = pictureNames + " recorder: ATTENTION! No images have been saved despite recorder beeing turned on: ";
                std::cerr << msg << fullAbsolutePath << std::endl;
            }
            else
            {
                std::string msg = pictureNames + " recorder: Recorder was turned off: ";
                std::cout << msg << fullAbsolutePath << std::endl;
            }
            
            if(IsDirectoryEmpty(fullAbsolutePath))
                RemoveDirectory(fullAbsolutePath);
        }
    }

    bool
    PictureSaver::IsDirectoryEmpty(const std::string &path)
    {
        //TODO: Implement.
        return false;
    }

    void
    PictureSaver::RemoveDirectory(const std::string &path)
    {
        //Too long on purpose. I don't want any problems with this ever.
        char commandPathBuffer[512];
        sprintf(commandPathBuffer, "rmdir %s", path.c_str());
        int status = system(commandPathBuffer); // Creating a directory

        if (status == -1)
            std::cerr << "Error : " << strerror(errno) << std::endl;
        else
            std::cout << "Folder removed." << std::endl;
    }
} // namespace Recorders
